<?php

// $personObj = (object) [
// 	'firstName' => "Senku",
// 	'middleName' => " ",
// 	'lastName' => "Ishigami"
// ];

	class Person {
		public $firstName;
		public $middleName;
		public $lastName;
		public $occupation;

		public function __construct($firstName, $middleName, $lastName, $occupation) {
			$this-> firstName = $firstName;
			$this-> middleName = $middleName;
			$this-> lastName = $lastName;
			$this-> occupation = $occupation;
		}

		public function printName() {
			return "Your fullname is $this->firstName $this->middleName $this->lastName";
		}
	}

	class Developer extends Person{
		public function printName(){
			return "Your name is $this->firstName $this->middleName $this->lastName and you are a $this->occupation";
		}
	}

	class Engineer extends Person{
		public function printName() {
		return "You are an $this->occupation named $this->firstName $this->middleName $this->lastName";
	}
}

$person = new Person('Senku', ' ', 'Ishigami', ' ');
$developer = new Developer('John', 'Finch', 'Smith', 'Developer');
$engineer = new Engineer('Harold', 'Myers', 'Reese', 'Engineer');